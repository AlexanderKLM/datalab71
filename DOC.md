
# Задачи:
    1. Для первой части организовать проект с системой контроля версий и систему сборки на основе CI/CD. Система CI/CD должна собирать приложение запускать его передовая аргумент, который может динамически указать пользователь. Результат работы приложения должен быть возвращен из CI/CD в виде артефакта.
    2. Для второй части организовать проект с системой контроля версий и систему сборки на основе CI/CD. Проект должен состоять из 2-х сервисов: сервиса «приложения» и сервиса с «данными». Сервис «приложения», должен делать запрос в сервис с «данными». Система CI/CD должна собирать приложение и подготовить все для запуска или получения результата его работы (Например, собрать и запустить сервисы в разных окружениях, где приложение делает запрос к данным и результат сохраняет в виде артефакта или подготовить файл для запуска сервисов «docker-compose.yml», которые сохраняют результат в файле).

## Часть 1. Вариант 7.
Создать программу по сравнению количества цифр после запятой у вещественных чисел, которые вводит пользователь.

## Часть 2. Вариант 1.
Составить список учебной группы, включающий Х человек. Для каждого студента указать: фамилию и имя, дату рождения (год, месяц и число), также у каждого студента есть зачетка, в которой указаны: предмет (от трех до пяти), дата экзамена, ФИО преподавателя.
Программа должна обладать следующим функционалом: Вывод в виде таблицы ФИО и даты рождения всех студентов группы 

## Требования к репозиторию
    1. Создать публичный репозиторий на gitlab
    2. Оформить структуру проекта на любом языке программирования и сделать первоначальную фиксацию пустого проекта
    3. Оформить ветки «master» и «dev»
    4. Зафиксировать в ветке «dev» промежуточные этапы разработки
    5. В ветку «master» зафиксировать версию 1.0. полностью готовое решение задачи
    6. Создать документацию:
        a. В корне проекта: файл README.md с кратким описанием и интерфейсом программы в разметке markdown.
        b. В корне проекта: файл DOC.md с описанием задачи в разметке markdown

## Функциональные требования:
    1. Решить задачи на любом языке программирования
    2. Решения всех задач должны собираться, запускаться или разворачиваться системой CI/CD
    3. Для 1 части, требуется ввод данных, организовать данный ввод динамически средствами CI/CD, а не «жестко закодировать»
    4. Для 2 части данные для задачи и приложение должны быть разделены на отдельные изолированные друг от друга сервисы. Например, сервис «БД» и сервис «приложения», которое делает запрос к «БД»
    5. Данные для части 2 могут храниться в файле или БД

## Порядок сдачи
Подготовить отчет в свободной форме по правилам оформления курсовой/практики, в котором должно быть:
    1. Исходный код решения
    2. Конфигурация CI/CD
    3. При наличии: файлы запуска сервисов (Dockerfile, docker-compose.yml и тд)
    4. Ссылка на проект в публичном репозитории Github/Gitlab или личная демонстрация проекта
